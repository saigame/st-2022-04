using System.Collections.Generic;
using UnityEngine;

public class BulletSpawner : Spawner
{
    public static BulletSpawner bulletInstance;

    protected override void Awake()
    {
        base.Awake();
        if (BulletSpawner.bulletInstance != null) Debug.LogError("Only 1 Spawner allow");
        BulletSpawner.bulletInstance = this;
    }

    protected override void LoadSpawnPos()
    {
       //For override
        //Debug.Log(transform.name + ": LoadSpawnPos", gameObject);
    }
}