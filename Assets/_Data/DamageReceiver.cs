using UnityEngine;

public class DamageReceiver : SaiMonoBehaviour
{
    [Header("DamageReceiver")]
    public int hp = 1;
    public int hpMax = 1;

    protected override void OnEnable()
    {
        base.OnEnable();
        this.Reborn();
    }

    public virtual int Deduct(int amount)
    {
        this.hp -= amount;
        this.DieCheck();
        return this.hp;
    }

    public virtual void SetDead()
    {
        this.Deduct(this.hp);
    }

    protected virtual void DieCheck()
    {
        if (!this.IsDead()) return;
        this.Destroy();
    }

    protected virtual void Destroy()
    {
        //Destroy(transform.parent.gameObject);//TODO: return to pool object
        Spawner.instance.Despawn(transform.parent);
    }

    public virtual bool IsDead()
    {
        return this.hp <= 0;
    }

    public virtual void Reborn()
    {
        string objName = transform.name;

        this.hp = this.hpMax;
    }
}