using System.Collections.Generic;
using UnityEngine;

public class BossSpawner: Spawner
{
    public static BossSpawner bossInstance;

    [Header("Boss")]
    [SerializeField] protected int killRequire = 10;

    protected override void Awake()
    {
        if (BossSpawner.bossInstance != null) Debug.LogError("Only 1 BossSpawner allow");
        BossSpawner.bossInstance = this;

        this.HideAll();
    }

    protected override void LoadComponents()
    {
        this.listSpawnPosName = "BossSpawnPos";
        this.spawnHolderName = "BossSpawnHolder";
        this.despawnHolderName = "BossDespawnHolder";
        this.spawnLimit = 1;
        this.spawnDelay = 5;

        base.LoadComponents();
    }

    protected override bool CanSpawn()
    {
        bool notMax = this.SpawnCount() < this.spawnLimit;
        bool isEnoughKill = this.killRequire <= ScoreManager.instance.GetKill();
        return notMax && isEnoughKill;
    }
}
