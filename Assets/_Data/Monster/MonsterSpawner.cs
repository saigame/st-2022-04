using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawner: Spawner
{
    public static MonsterSpawner monsterInstance;

    protected override void Awake()
    {
        if (MonsterSpawner.monsterInstance != null) Debug.LogError("Only 1 MonsterSpawner allow");
        MonsterSpawner.monsterInstance = this;

        this.HideAll();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        this.ClearMonsterOnBossApear();
    }

    protected override bool CanSpawn()
    {
        bool notMax = this.SpawnCount() < this.spawnLimit;
        bool noBoss = BossSpawner.bossInstance.SpawnCount() == 0;
        return notMax && noBoss;
    }

    protected virtual void ClearMonsterOnBossApear()
    {
        if (BossSpawner.bossInstance.SpawnCount() <= 0) return;

        foreach(Transform monster in this.spawnHolder)
        {
            MonsterCtrl monsterCtrl = monster.GetComponent<MonsterCtrl>();
            Debug.Log(monsterCtrl.name,monsterCtrl.gameObject);
            monsterCtrl.damageReceiver.SetDead();
        }
    }
}
