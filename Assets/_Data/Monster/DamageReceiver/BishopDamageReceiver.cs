using UnityEngine;

public class BishopDamageReceiver : MonsterDamageReceiver
{
    protected override void ResetValue()
    {
        base.ResetValue();
        this.killPoint = 4;
        this.hpMax = 7;
    }
}