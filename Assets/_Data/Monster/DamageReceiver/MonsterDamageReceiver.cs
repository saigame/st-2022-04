using UnityEngine;

public class MonsterDamageReceiver : DamageReceiver
{
    [Header("Monster")]
    [SerializeField] protected int killPoint = 1;
    [SerializeField] protected BossDefeated bossDefeated;

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadBossDefeated();
    }

    protected virtual void LoadBossDefeated()
    {
        if (this.bossDefeated != null) return;
        Transform obj = transform.Find("BossDefeated");
        if (obj == null) return;
        this.bossDefeated = obj.GetComponent<BossDefeated>();
        Debug.Log(transform.name + ": LoadBossDefeated", gameObject);
    }

    protected override void DieCheck()
    {
        if (!this.IsDead()) return;
        ScoreManager.instance.AddKill(this.killPoint);
        if (this.bossDefeated != null) this.bossDefeated.OnDefeated();
        this.Destroy();
    }

    protected override void Destroy()
    {
        MonsterSpawner.monsterInstance.Despawn(transform.parent);
    }
}