using UnityEngine;

public class RookDamageReceiver : MonsterDamageReceiver
{
    protected override void ResetValue()
    {
        base.ResetValue();
        this.killPoint = 2;
        this.hpMax = 2;
    }
}