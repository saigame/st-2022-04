using UnityEngine;

public class BossDefeated : SaiMonoBehaviour
{
    public virtual void OnDefeated()
    {
        ScoreManager.instance.SetBossKill();
    }
}