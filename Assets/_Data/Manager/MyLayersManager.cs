using UnityEngine;

public class MyLayersManager : SaiMonoBehaviour
{
    public static MyLayersManager instance;

    [Header("Layers")]
    public int layerPlayer;
    public int layerEnemy;

    protected override void Awake()
    {
        base.Awake();
        if (MyLayersManager.instance != null) Debug.LogError("Only 1 MyLayersManager allow");
        MyLayersManager.instance = this;

        Physics.IgnoreLayerCollision(MyLayersManager.instance.layerEnemy, MyLayersManager.instance.layerPlayer, true);
        Physics.IgnoreLayerCollision(MyLayersManager.instance.layerEnemy, MyLayersManager.instance.layerEnemy, true);
    }

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.GetPlayers();
    }

    protected virtual void GetPlayers()
    {
        if (this.layerPlayer < 1)
        {
            this.layerPlayer = LayerMask.NameToLayer("Player");
            if (this.layerPlayer < 0) Debug.LogError("Layer Player is mising");
        }

        if (this.layerEnemy < 1)
        {
            this.layerEnemy = LayerMask.NameToLayer("Enemy");
            if (this.layerEnemy < 0) Debug.LogError("Layer Enemy  is mising");
        }
    }

}