using UnityEngine;
using UnityEngine.SceneManagement;

public class MySceneManager : SaiMonoBehaviour
{
    public static MySceneManager instance;

    [SerializeField] protected bool isSceneOver = false;
    [SerializeField] protected int maxLevel = 4;

    protected override void Awake()
    {
        base.Awake();
        if (MySceneManager.instance != null) Debug.LogError("Only 1 SceneManager allow");
        MySceneManager.instance = this;
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        this.CheckSceneOver();
    }

    protected virtual void CheckSceneOver()
    {
        this.isSceneOver = true;
        if (ScoreManager.instance.GetBossKill() < 1) this.isSceneOver = false;
    }

    public virtual bool IsSceneOver()
    {
        return this.isSceneOver;
    }

    public virtual bool IsLastLevel()
    {
        int sceneId = this.GetSceneId();
        return sceneId >= this.maxLevel;
    }

    public virtual void NextScene()
    {
        int sceneId = this.GetSceneId();
        if (sceneId >= this.maxLevel) return;
        string sceneNext = "level_" + (sceneId + 1).ToString();
        SceneManager.LoadScene(sceneNext);
    }

    public virtual void ResetGame()
    {
        string sceneNext = "level_1";
        SceneManager.LoadScene(sceneNext);
    }

    public virtual int GetSceneId()
    {
        Scene scene = SceneManager.GetActiveScene();
        string sceneName = scene.name;
        return int.Parse(sceneName.Replace("level_", ""));
    }
}
