using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveGameManager : SaiMonoBehaviour
{
    public static SaveGameManager instance;
    public const string SCORE_NAME = "score";

    protected override void Awake()
    {
        if (SaveGameManager.instance != null) Debug.LogError("Only 1 SaveGameManager allow");
        SaveGameManager.instance = this;
    }

    protected override void Start()
    {
        base.Start();
        this.LoadSaveGame();

        InvokeRepeating("AutoSave", 1f, 5f);
    }

    protected virtual void AutoSave()
    {
        this.SaveMonsterScore();
    }

    public virtual void SaveMonsterScore()
    {
        SaveSystem.SetInt(SaveGameManager.SCORE_NAME, ScoreManager.instance.GetScore());
    }

    protected virtual void LoadSaveGame()
    {
        int score = SaveSystem.GetInt(SaveGameManager.SCORE_NAME, 0);
        ScoreManager.instance.SetScore(score);
    }
}
