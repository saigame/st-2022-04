using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;

    [Header("ScoreManager")]
    [SerializeField] protected int score = 0;
    [SerializeField] protected int monsterKill = 0;
    [SerializeField] protected int bossKill = 0;

    protected virtual void Awake()
    {
        if (ScoreManager.instance != null) Debug.LogError("Only 1 ScoreManager allow");
        ScoreManager.instance = this;
    }

    public virtual void SetScore(int score)
    {
        this.score = score;
    }

    public virtual int GetScore()
    {
        return this.score;
    }

    public virtual void AddKill(int add = 1)
    {
        this.score += add;
        this.monsterKill += add;
        SaveGameManager.instance.SaveMonsterScore();
    }

    public virtual int GetKill()
    {
        return this.monsterKill;
    }

    public virtual void SetBossKill(int add = 1)
    {
        this.bossKill += add;
    }

    public virtual int GetBossKill()
    {
        return this.bossKill;
    }

}
