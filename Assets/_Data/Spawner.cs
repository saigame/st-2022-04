using System.Collections.Generic;
using UnityEngine;

public abstract class Spawner : SaiMonoBehaviour
{
    public static Spawner instance;

    [Header("Obj Ref")]
    [SerializeField] protected Transform listSpawnPosObj;
    [SerializeField] protected string listSpawnPosName = "SpawnPos";
    [SerializeField] protected List<Transform> listSpawnPos;
    [Header(" ")]

    [SerializeField] protected Transform spawnHolder;
    [SerializeField] protected string spawnHolderName = "SpawnHolder";
    [SerializeField] protected List<Transform> prefabs;
    [Header(" ")]

    [SerializeField] protected Transform despawnHolder;
    [SerializeField] protected string despawnHolderName = "DespawnHolder";

    [Header("Spawner")]
    [SerializeField] protected int spawnCount = 0;
    [SerializeField] protected int spawnLimit = 10;
    [SerializeField] protected float spawnTimer = 0;
    [SerializeField] protected float spawnDelay = 1f;

    protected override void Awake()
    {
        if (Spawner.instance != null) Debug.LogError("Only 1 Spawner allow");
        Spawner.instance = this;

        this.HideAll();
        //InvokeRepeating("Spawning", 5f, 1f);
    }

    public static Spawner Instance()
    {
        return Spawner.instance;
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        this.Spawning();

        this.spawnCount = this.spawnHolder.childCount;
    }

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadSpawnPos();
        this.LoadObjects();
    }

    protected virtual void LoadSpawnPos()
    {
        if (this.listSpawnPos.Count > 0) return;
        this.listSpawnPosObj = GameObject.Find(this.listSpawnPosName).transform;
        this.spawnHolder = GameObject.Find(this.spawnHolderName).transform;
        this.despawnHolder = GameObject.Find(this.despawnHolderName).transform;
        foreach (Transform child in this.listSpawnPosObj)
        {
            this.listSpawnPos.Add(child);
        }

        Debug.Log(transform.name + ": LoadSpawnPos", gameObject);
    }

    protected virtual void LoadObjects()
    {
        if (this.prefabs.Count > 0) return;
        foreach (Transform child in transform)
        {
            this.prefabs.Add(child);
        }

        //this.HideAll();

        Debug.Log(transform.name + ": LoadObjects", gameObject);
    }

    protected virtual void HideAll()
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }
    }

    protected virtual void Spawning()
    {
        ///InvokeRepeating("Spawning", 5f, this.spawnDelay);
        if (MySceneManager.instance.IsSceneOver()) return;
        if (!this.CanSpawn()) return;

        this.spawnTimer += Time.fixedDeltaTime;
        if (this.spawnTimer < this.spawnDelay) return;
        this.spawnTimer = 0;

        //Debug.Log("Spawned");
        Transform prefab = this.GetObj2Spawn();
        Vector3 pos = this.GetPos2Spawn();
        Transform newObj = this.SpawnObj(prefab, pos, this.spawnHolder);
        newObj.gameObject.SetActive(true);
    }

    protected virtual bool CanSpawn()
    {
        return this.SpawnCount() < this.spawnLimit;
    }

    public virtual int SpawnCount()
    {
        return this.spawnHolder.childCount;
    }

    protected virtual Transform GetObj2Spawn()
    {
        int rand = Random.Range(0, this.prefabs.Count);
        return this.prefabs[rand];
    }

    protected virtual Vector3 GetPos2Spawn()
    {
        int rand = Random.Range(0, this.listSpawnPos.Count);
        return this.listSpawnPos[rand].position;
    }

    public virtual Transform SpawnObj(Transform prefab, Vector3 pos)
    {
        Transform spawnedObj = this.GetObjFromDespawnHolder(prefab);

        if (spawnedObj == null) spawnedObj = Instantiate(prefab);

        spawnedObj.name = prefab.name;
        spawnedObj.position = pos;
        return spawnedObj;
    }

    protected virtual Transform GetObjFromDespawnHolder(Transform prefab)
    {
        int childCount = this.despawnHolder.childCount;
        if (childCount < 1) return null;

        foreach (Transform child in this.despawnHolder)
        {
            if (child.name == prefab.name) return child;
        }

        return null;
    }

    public virtual Transform SpawnObj(Transform prefab, Vector3 pos, Transform parent)
    {
        Transform newObj = this.SpawnObj(prefab, pos);
        newObj.parent = parent;
        return newObj;
    }

    public virtual void Despawn(Transform obj)
    {
        obj.gameObject.SetActive(false);
        obj.parent = this.despawnHolder;
    }

    public virtual float SpawnCountDown()
    {
        float countDown = this.spawnDelay - this.spawnTimer;
        if (countDown == this.spawnDelay) countDown = -1;

        return countDown;
    }
}
