using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UISceneResetGame : MonoBehaviour
{
    public virtual void Reset()
    {
        MySceneManager.instance.ResetGame();
    }
}
