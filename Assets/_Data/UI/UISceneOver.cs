using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISceneOver : SaiMonoBehaviour
{
    public GameObject btnNext;
    public GameObject btnResetGame;

    protected override void Start()
    {
        base.Start();
        InvokeRepeating("CheckSceneOver", 0.1f, 0.5f);
    }

    protected virtual void CheckSceneOver()
    {
        Debug.Log("CheckSceneOver");
        if (MySceneManager.instance.IsSceneOver()) this.Show();
        else this.Hide();
    }

    protected virtual void Show()
    {
        gameObject.SetActive(true);
        if (MySceneManager.instance.IsLastLevel())
        {
            this.btnNext.SetActive(false);
            this.btnResetGame.SetActive(true);
        }
        else
        {
            this.btnNext.SetActive(true);
            this.btnResetGame.SetActive(false);
        }
    }

    protected virtual void Hide()
    {
        gameObject.SetActive(false);
    }
}
