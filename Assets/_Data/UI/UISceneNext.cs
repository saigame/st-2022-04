using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UISceneNext : MonoBehaviour
{
    public virtual void Next()
    {
        MySceneManager.instance.NextScene();
    }
}
