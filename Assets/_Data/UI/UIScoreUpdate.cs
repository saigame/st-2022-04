using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIScoreUpdate : MonoBehaviour
{
    public TextMeshProUGUI text;

    protected virtual void FixedUpdate()
    {
        this.text.text = ScoreManager.instance.GetScore().ToString();
    }
}
