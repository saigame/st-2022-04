using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIBossCountDown : MonoBehaviour
{
    public TextMeshProUGUI text;

    protected virtual void FixedUpdate()
    {
        float remain = BossSpawner.bossInstance.SpawnCountDown();
        remain = Mathf.RoundToInt(remain);
        if (remain == -1) this.text.text = "";
        else this.text.text = remain.ToString();
    }
}
