using UnityEngine;

public class PlayerMovement : CharCtrlMove
{
    public PlayerController playerController;

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadPlayerController();
        this.speed = 3f;
    }

    protected virtual void LoadPlayerController()
    {
        if (this.playerController != null) return;
        this.playerController = transform.parent.GetComponent<PlayerController>();
        Debug.Log(transform.name + ": LoadPlayerController", gameObject);
    }

    protected override Animator Animator()
    {
        return this.playerController.animator;
    }

    protected override Transform Model()
    {
        return this.playerController.playerModel;
    }

    protected override CharacterController CharacterController()
    {
        return this.playerController.characterController;
    }
}
