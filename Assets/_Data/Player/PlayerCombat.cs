using UnityEngine;

public class PlayerCombat : Combat
{
    public PlayerController playerController;

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadPlayerController();
    }

    protected virtual void LoadPlayerController()
    {
        if (this.playerController != null) return;
        this.playerController = transform.parent.GetComponent<PlayerController>();
        Debug.Log(transform.name + ": LoadPlayerController", gameObject);
    }

    public override void SpawnSkill()
    {
        if (this.skillReleased) return;
        this.skillReleased = true;

        //Debug.Log("SpawnSkill");

        Transform fx = FXManager.instance.Spawn("Fireball");
        fx.position = this.GetStrikePoint();
        FireballFly fireballFly = fx.GetComponent<FireballFly>();
        fireballFly.Turning(PlayerController.instance.playerMovement.isTurnRight);
        fx.gameObject.SetActive(true);
    }

    protected override Animator Animator()
    {
        return this.playerController.animator;
    }

    public override CharCtrlMove CharCtrlMove()
    {
        return this.playerController.playerMovement;
    }
}
