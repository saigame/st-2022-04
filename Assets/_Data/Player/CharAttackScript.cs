using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharAttackScript : MonoBehaviour
{
    public virtual void SpawmSkill()
    {
        //Debug.Log("SpawmSkill");
        PlayerController.instance.playerCombat.SpawnSkill();
    }

    public virtual void SkillFinished()
    {
        //Debug.Log("SkillFinished");
        PlayerController.instance.playerCombat.AttackFinish();
    }
}
