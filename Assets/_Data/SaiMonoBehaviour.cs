using UnityEngine;

public class SaiMonoBehaviour : MonoBehaviour
{
    protected virtual void Awake()
    {
        this.LoadComponents();
    }

    protected virtual void Start()
    {
        //For override
    }

    protected virtual void Reset()
    {
        this.ResetValue();
        this.LoadComponents();
    }

    protected virtual void ResetValue()
    {
        //For override
    }

    protected virtual void Update()
    {
        //For override
    }

    protected virtual void FixedUpdate()
    {
        //For override
    }

    protected virtual void OnEnable()
    {
        //For override
    }

    protected virtual void LoadComponents()
    {
        //For override   
    }
}
